<?php
namespace app\index\controller;

use think\Config;
use think\Controller;
use Youzan\Open\Client;
use Youzan\Open\Token;

class Index extends Controller
{

    private $clientId = 'c30d34d1d054d718a6';
    private $clientSecret = 'f2d72e23bae97071ee81ae45625e8bd4';

    private $accessToken;
    private $client;

    public function __construct()
    {
        $this->setAccessToken();
        $this->client = new Client($this->accessToken);
    }

    public function setAccessToken(){

        $token = new Token($this->clientId,$this->clientSecret);

        $type = 'silent';
        $keys['kdt_id'] = '907414';

        $responseData = $token->getToken($type,$keys);

        $this->accessToken = $responseData['access_token'];
    }

    /**
     * 上传图片
     */
    private function uploadImage(){

        $method = 'youzan.materials.storage.platform.img.upload';
        $apiVersion = '3.0.0';

        $img = config('img_dir') . 'logo.jpeg';

        $params = [

        ];

// 一次仅支持上传一张图片
        $files = [
            'image' => $img
        ];

        $response = $this->client->post($method, $apiVersion, $params , $files);
        var_dump($response);
        return $response['data'];
    }

    public function addGoods()
    {
        $method = 'youzan.item.create';
        $apiVersion = '3.0.0';

        $imageData = $this->uploadImage();
var_dump($imageData['image_id']);
        $params = [
            'title' =>  '测试商品',
            'price' =>  '1',
            'image_ids' =>  "{$imageData['image_id']}",
            'desc' =>  '4003534',
        ];

        $response = $this->client->post($method, $apiVersion, $params);
        var_dump($response);
    }


}
